import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/observable';
import * as fromStore  from '../store';
import { Book } from "../model/book.model";

 @Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  books$: Observable<Book[]>;
  loading$: Observable<Boolean>;
  loadFail$: Observable<Boolean>;
  loaded$: Observable<Boolean>;
  loadingFailMessage: string;
  constructor(private store:Store<fromStore.ProductState>) { 

  }

  ngOnInit() {
    this.loading$ = this.store.select(fromStore.getAllBooksLoading);
    this.loadFail$ = this.store.select(fromStore.getAllBooksLodingError);
    this.books$ = this.store.select(fromStore.getAllBooks);
    this.loaded$ = this.store.select(fromStore.getAllBooksLoaded);
  }

}
