import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/observable';
import * as fromStore  from './store';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private store:Store<fromStore.ProductState>) { }
  ngOnInit() {
    this.store.dispatch(new fromStore.LoadBook());
  }
}
