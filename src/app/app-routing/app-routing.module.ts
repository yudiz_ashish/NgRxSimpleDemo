import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ResultComponent } from '../result/result.component';
import { InsertComponent } from '../insert/insert.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '', component: ResultComponent
      },
      {
        path: 'add', component: InsertComponent
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
