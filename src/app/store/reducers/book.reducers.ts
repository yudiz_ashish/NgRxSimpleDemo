import * as fromBook from '../actions/book.action';
import { Book } from "../../model/book.model";

export interface BookState {
    data: Book[];
    loaded: boolean;
    loading: boolean;
    loadingError: boolean;
}

export const intialState : BookState = {
    data: [],
    loading: false,
    loaded: false,
    loadingError: false
}

export function reducer (
    state = intialState, 
    action: fromBook.BookAction
): BookState {
    switch(action.type){
        case fromBook.LOAD_BOOK : {
            return {
                ...state,
                loading: true,
                loaded: false,
                loadingError: false
            }
        }
        case fromBook.LOAD_BOOK_SUCCESS : {
            const data = action.payLoad;
            return {
                ...state,
                loading: false,
                loaded: true,
                loadingError: false,
                data
            }
        }
        case fromBook.LOAD_BOOK_FAIL : {
            const data = action.payLoad;
            console.log(data['error']);
            return {
                ...state,
                loaded: false,
                loading: false,
                loadingError: true,
                data
            }
        }
    }
    return state;
}

export const getBookLoding = (state:BookState ) => state.loading;
export const getBookLoaded = (state:BookState ) => state.loaded;
export const getBookLoadingError = (state:BookState) => state.loadingError;
export const getBook = (state:BookState ) => state.data;