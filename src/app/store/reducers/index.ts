import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromBook from './book.reducers';
export interface ProductState {
    books: fromBook.BookState
};

export const reducers: ActionReducerMap<ProductState> = {
    books : fromBook.reducer
};

export const getProductState = createFeatureSelector<ProductState>("products");

//Book State

export const getBookState = createSelector(
    getProductState, 
    (state: ProductState) => state.books
);

export const getAllBooksLoading = createSelector(getBookState, fromBook.getBookLoding);
export const getAllBooksLoaded = createSelector(getBookState, fromBook.getBookLoaded);
export const getAllBooksLodingError = createSelector(getBookState,fromBook.getBookLoadingError);
export const getAllBooks = createSelector(getBookState, fromBook.getBook);