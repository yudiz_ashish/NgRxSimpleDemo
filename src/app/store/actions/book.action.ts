import { Action } from '@ngrx/store';
import { Book } from '../../model/book.model';

//Load Book
export const LOAD_BOOK = '[Item] Load Books';
export const LOAD_BOOK_SUCCESS = '[Item] Load Books Success';
export const LOAD_BOOK_FAIL = '[Item] Load Books Fail';

export class LoadBook implements Action {
    readonly type = LOAD_BOOK;
}

export class LoadBookSuccess implements Action {
    readonly type = LOAD_BOOK_SUCCESS;
    constructor(public payLoad : Book[]){}
}

export class LoadBookFail implements Action {
    readonly type = LOAD_BOOK_FAIL;
    constructor(public payLoad:any){}
}

//Export Action Type

export type BookAction = LoadBook | LoadBookSuccess | LoadBookFail;