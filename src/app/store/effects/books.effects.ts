import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import * as booksActions from '../actions/book.action';
import { switchMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import * as fromServices from '../../services'
@Injectable()
export class BookEffects {
    constructor(
        private action$:Actions,
        private bookServices: fromServices.BookService
    ) {}
    // loadBooks$ = this.action$.ofType(booksActions.LOAD_BOOK);

    @Effect()
    loadBooks$ = this.action$
                    .ofType(booksActions.LOAD_BOOK)
                    .pipe(
                        switchMap(() => {
                            return this.bookServices.getBooks().pipe(
                                map(books => new booksActions.LoadBookSuccess(books)),
                                catchError(error =>  of(new booksActions.LoadBookFail(error)))
                            )  
                        })
                    )
}