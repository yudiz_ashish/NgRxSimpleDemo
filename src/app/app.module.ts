import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatInputModule} from '@angular/material';
import {AppComponent} from './app.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import { ResultComponent } from './result/result.component';
import { InsertComponent } from './insert/insert.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { StoreModule } from '@ngrx/store';
import {EffectsModule } from '@ngrx/effects';
import { reducers, effects } from './store';
import { service } from './services';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    ResultComponent,
    InsertComponent
  ],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature('products',reducers),
    EffectsModule.forRoot([]),
    EffectsModule.forFeature(effects),
    BrowserModule, 
    BrowserAnimationsModule, 
    MatButtonModule, 
    MatCheckboxModule, 
    MatToolbarModule, 
    MatFormFieldModule, 
    MatInputModule, 
    MatExpansionModule,
    MatListModule, 
    MatSidenavModule
  ],
  providers: [
    service
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
