import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { Book } from '../model/book.model';

@Injectable()
export class BookService {
    constructor(private http:HttpClient){}

    getBooks():Observable<Book[]>{
        return this.http
        .get<Book[]>('http://192.168.10.114:4000/api/v1/book/getbook')
        .pipe(catchError((error: any) => Observable.throw(error)));
    }
}